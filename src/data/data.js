export const data = [
    {
        id: 1,
        type: "Home",
        title: "Gérer vos ruches connectêes simplement",
        date: "Janvier 2021",
        Description: "Notre solution propose de gérer vos ruchers à distance et d’émettre des alertes en cas de danger imminent.\n La précision des capteurs au sein des ruches connectées assurera une détection efficace.",
        img: "/Users/aguegdjou/Documents/ruche-connectees/public/assets/1-Page_accueil_abeilles.jpg",
        link: "http://www.google.com",
    },
    {
        id: 2,
        title: "Lyon Béton",
        date: "Mars 2020",
        languages: ["Symfony", "Vue"],
        infos:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
        img: "./assets/img/projet-1.jpg",
        link: "http://www.google.com",
    },
    {
        id: 3,
        title: "Everpost",
        date: "Avril 2020",
        languages: ["Wordpress", "Php", "React"],
        infos:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
        img: "./assets/img/projet-3.jpg",
        link: "http://www.google.com",
    },
    {
        id: 4,
        title: "Creative Dev",
        date: "Juillet 2020",
        languages: ["Vue", "Php"],
        infos:
            "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quas cumque labore suscipit, pariatur laboriosam autem omnis saepe quisquam enim iste.",
        img: "./assets/img/projet-4.jpg",
        link: "http://www.google.com",
    },
];
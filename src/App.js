import React from "react";
import { Switch, Route, Link, Redirect } from "react-router-dom";

// Le routeur pour la navigation

import Home from "./pages/Home";
import Projet from "./pages/Projet";
import Actu from "./pages/Actu";
import Aide from "./pages/Aide";
import Contact from "./pages/Contact";
import Connexion from "./pages/Connexion";
import Inscription from "./pages/Inscription";

const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>

        <Route exact path="/Projet">
          <Projet />
        </Route>

        <Route exact path="/Actu">
          <Actu />
        </Route>

        <Route exact path="/Aide">
          <Aide />
        </Route>

        <Route exact path="/Contact">
          <Contact />
        </Route>

        <Route exact path="/Connexion">
          <Connexion />
        </Route>
        <Route exact path="/Inscription">
          <Inscription />
        </Route>
        <Redirect to="/" />
      </Switch>
    </div>
  );
};

export default App;

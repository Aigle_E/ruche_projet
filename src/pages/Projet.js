import React from "react";
import Footer from "../components/Footer";
import Navigation from "../components/Navigation";
import img1 from "../assets/avatar_G.jpg";
import img2 from "../assets/5-Photo_LABCESI.JPG";
import img3 from "../assets/9-Photo_decoupage3.JPG";
import img4 from "../assets/2-Photo_Sylas.JPG";
import img5 from "../assets/3-Photo_Lynda.JPG";
import img6 from "../assets/6-Photo_decoupage1.JPG";
import img7 from "../assets/7-Photo_decoupage2.JPG";
import img8 from "../assets/5-Photo_LABCESI.JPG";
import img9 from "../assets/8-Fab_Lab_logo.png";

const Projet = () => {
  const data = [
    {
      id: 1,
      type: "projet",
      title: "Présentation de l’équipe",
      date: "Janvier 2021",
      Description: "- Les membres de l’équipe projet sont :",
      Description1:
        "- Karine Brifault, formatrice pédagogique du CESI, qui suit la conception du projet et s’assure de sa qualité ;",
      Description2:
        "- Alexis Seurin, chef de projet, qui pilote le projet, responsable développement Raspberry Pi ;",
      Description3: "- Sylas Aguedjou, Architecte logiciel;",
      Description4: "- Linda Saibi, ... ;",
      Description5: "- Sydne Tavares, ... ;",
      Description6: "- Si Mohammed , ... ;",
    },
    {
      id: 2,
      type: "projet",
      title: "Descriptif du projet",
      date: "Janvier 2021",
      Description:
        "Le projet « Système de gestion de ruches connectées » vise à apporter notre aide aux apiculteurs. Nous allons construire une ruche, développer une application Web, mettre en place un serveur et faire de la métrologie sur la vie des colonies d’abeilles du rucher. Le but est de développer un système complet de gestion de ruches connectées sur une application Web sécurisée.",
      Description1:
        "Actuellement, les associations et les apiculteurs gèrent manuellement leurs ruches. Or, la visite des ruches se fait une fois par mois pour éviter de gêner et stresser les abeilles. Cette gestion peut amener à perdre des ruches qui auraient pu être sauvées si une alerte avait été émise et si une action avait été effectuée immédiatement.",
      S_title: ["☖ Sécurité", "✥ Expérience client", "✔ Qualité"],
      S_description: [
        "Notre architecture est conçue pour assurer la sécurité et empêcher les intrusions.",
        "Besoin d'aide ? Ne vous inquiétez pas, notre équipe est là pour répondre à toutes vos questions.",
        "Nous serons attentifs et responsables de fournir la meilleure expérience utilisateur en ligne.",
      ],
    },
    {
      id: 3,
      type: "projet",
      title: "Nos réalisations",
      date: "Janvier 2021",
      Description:
        "Notre solution propose de gérer vos ruchers à distance et d’émettre des alertes en cas de danger imminent. \n \n La précision des capteurs au sein des ruches connectées assurera une détection efficace.",
    },
    {
      id: 4,
      type: "projet",
      title: "Nos partenaires",
      date: "Janvier 2021",
      Description: "",
    },
  ];

  return (
    <div className="wrapper">
      <Navigation />

      <main>
        <div className="row">
          <div className="divehome">
            <h1>Le projet</h1>
            <h3>{data[0].title}</h3>
            <p>{data[0].Description}</p>
            <p>{data[0].Description1}</p>
            <p>{data[0].Description2}</p>
            <p>{data[0].Description3}</p>
            <p>{data[0].Description4} </p>
            <p>{data[0].Description5}</p>
            <p>{data[0].Description6}</p>
          </div>

          <div className="divehome">
            <br />
            <br />
            <img src={img1} alt="abeilles" />
          </div>
        </div>
        <div className="row">
          <div className="divehome">
            <h2>{data[1].title}</h2>
            <table>
              <tr>
                <td>
                  <img id="img" src={img2} alt="lab" />
                </td>
              </tr>
              <tr>
                <td>
                  <img id="img" src={img3} alt="lab" />
                </td>
              </tr>
            </table>
          </div>

          <div className="divehome">
            <br />
            <br />
            <br />
            <br />
            <br />
            <p>{data[1].Description}</p>
            <p>{data[1].Description1}</p>
            <div className="divehome">
              <br />
              <br />
              <br />
              <div className="box" id="box_projrt6">
                {data[1].S_title.map((item) => {
                  return <div key={item}>{item}</div>;
                })}

                {data[1].S_description.map((item) => {
                  return <div key={item}>{item}</div>;
                })}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="divehome">
            <h2>{data[2].title}</h2>
            <p>{data[2].Description}</p>
            <p>{data[2].Description}</p>
          </div>

          <div className="divehome">
            <br />
            <br />
            <br />
            <table>
              <tr>
                <td>
                  <img id="img" src={img4} alt="lab" />
                </td>
                <td>
                  <img id="img" src={img5} alt="lab" />
                </td>
              </tr>
              <tr>
                <td>
                  <img id="img" src={img6} alt="lab" />
                </td>
                <td>
                  <img id="img" src={img7} alt="lab" />
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div className="row">
          <div className="divehome">
            <h2>{data[3].title}</h2>
            <img id="img" src={img8} alt="lab" />
          </div>

          <div className="divehome">
            <br />
            <br />
            <br />
            <br />
            <img id="img_lab" src={img9} alt="lab" />
          </div>
        </div>
      </main>

      <Footer />
    </div>
  );
};

export default Projet;

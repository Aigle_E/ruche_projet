import React, { useState } from 'react'
import Navigation from '../components/Navigation'
import Footer from "../components/Footer";
import imgR from "../assets/1-Page_accueil_abeilles.jpg";


const Home = () => {
    // const [currentProject] = useState(data);
    const data = {
        id: 1,
        type: "Home",
        title: "Gérer vos ruches connectées simplement",
        date: "Janvier 2021",
        Description: "Notre solution propose de gérer vos ruchers à distance et d’émettre des alertes en cas de danger imminent. \n \n La précision des capteurs au sein des ruches connectées assurera une détection efficace.",
        img: "/Users/aguegdjou/Documents/ruche-connectees/public/assets/1-Page_accueil_abeilles.jpg",
        link: "http://www.google.com",
    };
    // const data = useState(data)
    
    return (
        <div className="wrapper">

            <Navigation />

            <main>
                <div className="row">

                    <div className="divehome">
                        <h1>{data.title}</h1>
                        <p>{data.Description}</p>
                        <p>{data.Description}</p>
                    </div>

                    <div className="divehome">
                        <img src={imgR} alt="abeilles" />
                    </div>
                </div>
            </main>

            <Footer />


        </div>
    );
};

export default Home;
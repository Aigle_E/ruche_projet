import React from 'react'
import { useState } from "react";
import Footer from '../components/Footer';
import Navigation from '../components/Navigation';

const Contact = () => {
    const [name, setName] = useState("");
  const [company, setCompany] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    let nameS = document.getElementById("name");
    let emailS = document.getElementById("email");
    let messageS = document.getElementById("message");
    let formMess = document.querySelector(".form-message");

    const isEmail = () => {
      let isMail = document.getElementById("not-mail");
      let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

      if (email.match(regex)) {
        isMail.style.display = "none";
        return true;
      } else {
        isMail.style.display = "block";
        isMail.style.animation = "dongle 1s";
        setTimeout(() => {
          isMail.style.animation = "none";
        }, 1000);
        return false;
      }
    };

    if (name && isEmail() && message) {
      const templateId = "template_aofmtvBG";

      nameS.classList.remove("red");
      emailS.classList.remove("red");
      messageS.classList.remove("red");

    } else {
      formMess.innerHTML = "Merci de remplir correctement les champs requis *";
      formMess.style.background = "rgb(253, 87, 87)";
      formMess.style.opacity = "1";

      if (!name) {
        nameS.classList.add("error");
      }
      if (!email) {
        emailS.classList.add("error");
      }
      if (!message) {
        messageS.classList.add("error");
      }
    }
  };




    return (
        <div className="wrapper">

            <Navigation/>
            <main>
            <br/>
            <div className="login-box">
                <p>Contactez-nous</p>
                <br/>
                <form>
                    <div className="user-box">
                        <input
                        type="text"
                        id="name"
                        name="name"
                        required
                        onChange={(e) => setName(e.target.value)}
                        placeholder=""
                        value={name}
                        />
                        <label>Nom*</label>
                    </div>
                    <div className="user-box">
                        <input
                        type="text"
                        id="company"
                        name="company"
                        onChange={(e) => setCompany(e.target.value)}
                        placeholder=""
                        required
                        value={company}
                        />
                        <label>Société</label>
                    </div>
                   
                    <div className="user-box">
                        <input
                        type="text"
                        id="phone"
                        name="phone"
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder=""
                        required
                        value={phone}
                    />
                    <label>phone</label>
                    </div>

                    <div className="user-box">
                        {/* <div className="email-content"> */}
                            <label id="not-mail">Email non valide</label>
                            <input
                                type="mail"
                                id="email"
                                name="email"
                                required
                                onChange={(e) => setEmail(e.target.value)}
                                // placeholder="email *"
                                value={email}
                            />
                        {/* </div>     */}
                        <label>E-mail*</label>
                    </div>

                    <div className="user-box">
                    <textarea
                    id="message"
                    name="message"
                    onChange={(e) => setMessage(e.target.value)}
                    placeholder=" message *"
                    value={message}
                    required
                    />
                    </div>

                    <br/>
                    <div className="user-box">
                        <input
                        className="button hover"
                        type="submit"
                        value="envoyer"
                        id="E-contact"
                        onClick={handleSubmit}
                         />
                         
                    </div>

                    <div className="user-box">
                    <div className="form-message"></div>
                    </div>
                        {/* <a href="#">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        ENVOYER LE MESSAGE
                        </a> */}
                </form>
                </div>
                <br/>
            </main>   

            <Footer/>
             
        </div> 
    );

};

export default Contact;
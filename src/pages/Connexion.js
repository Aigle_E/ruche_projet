import React from "react";
import { useState } from "react";
import Footer from "../components/Footer";
import Navigation from "../components/Navigation";

const Connexion = () => {
  // dans le rocher on na la variable et pour la mettre ajoutre on ne peux que passer par
  // le set present dans le crocher de la variable
  //  au debut on lui attribut un valeur vide avec useState("")
  const [name, setName] = useState("");
  const [company, setCompany] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    let nameS = document.getElementById("name");
    let emailS = document.getElementById("email");
    let messageS = document.getElementById("message");
    let formMess = document.querySelector(".form-message");

    const isEmail = () => {
      let isMail = document.getElementById("not-mail");
      let regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

      if (email.match(regex)) {
        isMail.style.display = "none";
        return true;
      } else {
        isMail.style.display = "block";
        isMail.style.animation = "dongle 1s";
        setTimeout(() => {
          isMail.style.animation = "none";
        }, 1000);
        return false;
      }
    };

    if (name && isEmail() && message) {
      const templateId = "template_aofmtvBG";

      nameS.classList.remove("red");
      emailS.classList.remove("red");
      messageS.classList.remove("red");
    } else {
      formMess.innerHTML = "Merci de remplir correctement les champs requis *";
      formMess.style.background = "rgb(253, 87, 87)";
      formMess.style.opacity = "1";

      if (!name) {
        nameS.classList.add("error");
      }
      if (!email) {
        emailS.classList.add("error");
      }
      if (!message) {
        messageS.classList.add("error");
      }
    }
  };

  return (
    <div className="wrapper">
      <Navigation />
      <main>
        <br />
        <div className="login-box">
          <p>Connexion</p>
          <br />
          <form>
            <div className="user-box">
              <input type="text" name="" required="" />
              <label>Username</label>
            </div>
            <div className="user-box">
              <input type="password" name="" required="" />
              <label>Password</label>
            </div>
            <a href="#">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              SE CONNECTER
            </a>
          </form>
        </div>
        <br />
      </main>

      <Footer />
    </div>
  );
};

export default Connexion;

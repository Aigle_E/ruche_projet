import React from "react";
import imgF from "../assets/facebook.png";
import imgT from "../assets/Twitter.png";
import imgI from "../assets/Instagram.png";

const Footer = () => {
  return (
    // <div className="fixed-footer">
    //     <div className="footer-clean">
    <footer className="footer-clean">
      <div className="container">
        <div id="main">
          <div>
            <h3>Le projet</h3>
            <ul>
              <li>
                <a href="">Presentation de l'équipe</a>
              </li>
              <li>
                <a href="">Descriptif du projet</a>
              </li>
              <li>
                <a href="">Nos réalisations</a>
              </li>
              <li>
                <a href="">Nos partenaires</a>
              </li>
            </ul>
          </div>
          <div>
            <h3>Notre actualité</h3>
            <ul>
              <li>
                <a href="">Agenda</a>
              </li>
              <li>
                <a href="">Team</a>
              </li>
              <li>
                <a href="">Legacy</a>
              </li>
            </ul>
          </div>
          <div>
            <h3>Contact</h3>
            <ul>
              <li>
                <a href="">Job openings</a>
              </li>
              <li>
                <a href="">Alexis Seurin</a>
              </li>
              <li>
                <a href="">+33 6 80 67 96 05</a>
              </li>
              <li>
                <a href="">Contribuer au projet</a>
              </li>
            </ul>
          </div>

          <div>
            <h3>Aide</h3>
            <ul>
              <li>
                <a href="">Les questions courantes</a>
              </li>
              <li>
                <a href="">Recevoir votre ruche</a>
              </li>
              <li>
                <a href="">Wiki</a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className="item social">
        <a href="#">
          <img src={imgF} alt="F" />
        </a>
        <a href="#">
          <img src={imgT} alt="T" />
        </a>
        <a href="#">
          <img src={imgI} alt="I" />
        </a>

        <hr />
        <p className="copyright">Company Name © 2021</p>
      </div>
    </footer>
    //     </div>
    // </div>
  );
};

export default Footer;

import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = () => {
  return (
    <header>
      <div className="wrapper">
        <div className="fixed-header">
          <ul>
            <NavLink to="/" exact className="hover">
              <li className="thicker">Accueil</li>
            </NavLink>

            <NavLink to="/Projet" exact>
              <li className="act">Le projet</li>
            </NavLink>

            <NavLink to="/Actu" exact>
              <li className="act">Notre actualité</li>
            </NavLink>

            <NavLink to="/Aide" exact activeClassName="nav-active">
              <li className="act">Aide</li>
            </NavLink>

            <NavLink to="/Contact" exact>
              <li className="act">Contact</li>
            </NavLink>

            <NavLink to="/Inscription" exact>
              <li className="pos">
                <button>S'INSCRIRE</button>
                {/* <button className="button button2"></button> */}
              </li>
            </NavLink>

            <NavLink to="/Connexion" exact>
              <li className="pos">
                <button>CONNEXION</button>

                {/* <button className="button button2"></button> */}
              </li>
            </NavLink>
          </ul>
        </div>
      </div>
    </header>
  );
};

export default Navigation;

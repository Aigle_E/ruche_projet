// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
// import '@testing-library/jest-dom';
// npm install --save-dev dirty-chai

/* Pour les texte voici les import :
 -  npm install --save-dev dirty-chai    
 -  npm install --save-dev chai-jest-diff
 -  npm i chai     
 l'adaptateur de test avec ENZYME
  - npm install --save-dev enzyme enzyme-adapter-react-16 react-test-renderer chai-enzyme@beta
  Pour lancer "npm test"
 */

import Adapter from "enzyme-adapter-react-16";
import chai from "chai";
import { configure as configureEnzyme } from "enzyme";
import createChaiEnzyme from "chai-enzyme";
import createChaiJestDiff from "chai-jest-diff";
import dirtyChai from "dirty-chai";

chai.use(dirtyChai).use(createChaiJestDiff()).use(createChaiEnzyme());

configureEnzyme({ adapter: new Adapter() });
